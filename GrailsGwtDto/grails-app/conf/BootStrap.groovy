class BootStrap {
	
	def init = { servletContext ->
		def author1 = new Author(firstName: "Jan", lastName: "Ehrhardt")
		def book1 = new Book(title: "GwtTutorial")
		author1.addToBooks(book1)
		
		author1.save()
	}
	
	def destroy = {
	}
} 