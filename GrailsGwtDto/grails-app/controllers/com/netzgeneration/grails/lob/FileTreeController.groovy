package com.netzgeneration.grails.lob

import groovy.xml.MarkupBuilder;

class FileTreeController {
	
	static defaultAction = "view"
	
	// show.gsp will be used
	def show = {
	}
	
	// try on using yui directly
	// see http://cheztog.blogspot.com/2008/06/yui-treeview-grails.html
	def dir = {
		def cmsId = params.cms
		if (cmsId == null) cmsId = ''
		
		def dir = params.dir?:"/"
		log.debug('fileTree.dir: dir=' + dir)
		
		// Removing some characters from the path
		dir = dir.replaceAll("\\.\\./","")
		
		// Get the root dir for the CMS
		String configDir = '.' // grailsApplication.config.cmsdata.dir
		
		String target = configDir + cmsId + dir
		log.debug('fileTree.dir: target=' + target)
		
		def files = []
		File d = new File(target)
		if (d.exists() && d.isDirectory()){
			files = d.listFiles()
		}
		log.debug('fileTree.dir: files=' + files)
		
		// return back a JSON structure
		response.setHeader("Cache-Control", "no-store")
		render(contentType: "text/json") {
			nodes {
				for (f in files) {
					node(name: f.name, isDir: f.isDirectory())
				}
			}
		}
	}
	
	// try on RichUI plugin 
	// see http://www.grails.org/RichUI+Plugin#TreeView
	def list = { 
		def writer = new StringWriter() 
		def xml = new MarkupBuilder(writer) 
		xml.person(name: "John Doe"){ 
			books(name: "Books") { 
				book(name:"Book 1") 
				//Optional id 
				book(name:"Book 2", id: 1) 
			} 
		}
		if (!params.max) params.max = 10 
		[ data: writer.toString(), action: 'view'] 
	} 
	
	def view = list
	
}
