package com.netzgeneration.grails.lob

import java.io.File;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import com.netzgeneration.grails.util.WebUtil;
import com.solution51.sfu.SuperFileUploadService;

class UploadController {
	
	static defaultAction = "save"
	
	SuperFileUploadService superFileUploadService
	
	def save = {
		final String uploadFilename = params.uploadedFileId
		final MultipartFile file
		// see http://www.grails.org/plugin/super-file-upload
		if (uploadFilename) { 
			final def mp = request.fileMap
			log.debug("upload.save: FileMap is " + mp)
			// get the full path name of the file from the temp directory 
			file = new FileAsMultipartFile(superFileUploadService.getTempUploadFile(uploadFilename), null)
		} 
		// see http://www.grails.org/Controllers+-+File+Uploads
		else { 
			// file was not uploaded by flash. User might have javascript off 
			// file = null
			// handle normal file upload as per grails docs 
			if (request instanceof MultipartRequest)
				file = request.getFile('sfuFile')
			else 
				file = null
		}
		if (file) {
			if (!file.empty) {
				flash.message = 'upload done'
				// f.transferTo( new File('someotherloc') )
				// response.sendError(200,'Done');
			}
			else {
				flash.message = 'file is empty'
			}
		}    
		else {
			flash.message = 'ready to upload'
			// redirect(action:'uploadForm')
		}
		log.debug("upload.save: request is " + WebUtil.requestToString(request))
		log.debug("upload.save: " + file)
	}
}
