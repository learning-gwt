class BookRpcService {
	
	boolean transactional = true
	
	static expose = ["gwt:org.grails.gwttutorial.client.rpc"]
	
	String getAuthor(int id) {
		def author = Author.get(id)
    	// out << "BookRpcService called with id="
		return author.encodeAsJSON()
	}
	
}
