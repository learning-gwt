import java.io.File;

import com.lazywithclass.client.rpc.TransferObject;

class SimpleRpcService {

    boolean transactional = true
    
    static expose = ['gwt:com.lazywithclass.client.rpc']

    String callme(TransferObject to) {
    	System.out.println("ok, service called")
    	System.out.println(to.getBar());
		System.out.println(to.getBaz());
		System.out.println(to.getFoo());

		return "this is from the server side"
    }
}
