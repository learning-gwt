<html>
<head>
<title>Simple form test</title>
</head>
<resource:treeView remote="false" />

<!-- 
	Dependency for YAHOO.util.Connect 
	see http://developer.yahoo.com/yui/connection/
-->
<script src="http://yui.yahooapis.com/2.8.0r4/build/yahoo/yahoo-min.js"></script>
 
<!-- Used for Custom Events and event listener bindings -->
<script src="http://yui.yahooapis.com/2.8.0r4/build/event/event-min.js"></script>
 
<!-- Source file -->
<!--
	If you require only basic HTTP transaction support, use the
	connection_core.js file.
-->
<script src="http://yui.yahooapis.com/2.8.0r4/build/connection/connection_core-min.js"></script>
 
<!--
	Use the full connection.js if you require the following features:
	- Form serialization.
	- File Upload using the iframe transport.
	- Cross-domain(XDR) transactions.
<script src="http://yui.yahooapis.com/2.8.0r4/build/connection/connection-min.js"></script>
-->
 
<!-- Dependencies for JSON -->
<script src="http://yui.yahooapis.com/2.8.0r4/build/yahoo/yahoo-min.js"></script>
 
<!-- Source file -->
<script src="http://yui.yahooapis.com/2.8.0r4/build/json/json-min.js"></script>

<body>

<h1><g:message code="default.create.label" args="[entityName]" /></h1>
<g:if test="${flash.message}">
	<div class="message">
	${flash.message}
	</div>
</g:if>
<g:hasErrors bean="${userInstance}">
	<div class="errors"><g:renderErrors bean="${userInstance}"
		as="list" /></div>
</g:hasErrors>

<richui:treeView xml="${data}" />
<br />
<div id="treeDiv"/>
<!-- stolen from http://cheztog.blogspot.com/2008/06/yui-treeview-grails.html -->
<g:javascript>
(function(){

// Creating the tree
function buildTree() {
    var tree;
    tree = new YAHOO.widget.TreeView("treeDiv");
    tree.setDynamicLoad(loadNodeData);
    var root = tree.getRoot();
    var tempNode = new YAHOO.widget.TextNode("/", root, false);
    tree.draw();
}

// Processing new nodes
function loadNodeData(node, fnLoadComplete) {
    var path = "";
    var leafNode = node;
    while (!leafNode.isRoot()) {
        path = leafNode.label + "/" + path;
        leafNode = leafNode.parent;
    }

    var nodelabel = encodeURI(path);
    // var sUrl = "${params.cms}/dir?dir="+nodelabel;
    // var sUrl = document.URL + "/../dir?dir="+nodelabel;
    var sUrl = "dir?dir="+nodelabel;
    // alert(sUrl);
    var callback = {
         success: function(oResponse){
             // alert("success: " + oResponse);
             var oResults = YAHOO.lang.JSON.parse(oResponse.responseText);
             if ((oResults.nodes)&&(oResults.nodes.length)){
                 if (YAHOO.lang.isArray(oResults.nodes)) {
                   for (var i=0; i < oResults.nodes.length; i++){
                          var tempNode = new YAHOO.widget.TextNode(oResults.nodes[i].name, node, false);
                          tempNode.isLeaf = !oResults.nodes[i].isDir;
                      }
                  }
              }
              oResponse.argument.fnLoadComplete();
          },
          failure: function(oResponse){
          	  // alert("failure:" + oResponse);
              oResponse.argument.fnLoadComplete();
          },
          argument:{
              "node": node,
              "fnLoadComplete":fnLoadComplete
          },
          timeout:5000
      }
      YAHOO.util.Connect.asyncRequest('GET', sUrl, callback);
  };
  // alert("Ready");
  YAHOO.util.Event.onDOMReady(buildTree);   
}());
</g:javascript>

</body>
</html>