<html>
<head>
<title>Simple form test</title>
<sfu:generateConfiguration fileSize="30" form="uploadForm"
	buttonImageFile="/buttons/upload_button.png" />
</head>

<body>

<h1><g:message code="default.create.label" args="[entityName]" /></h1>
<g:if test="${flash.message}">
	<div class="message">
	${flash.message}
	</div>
</g:if>
<g:hasErrors bean="${userInstance}">
	<div class="errors"><g:renderErrors bean="${userInstance}"
		as="list" /></div>
</g:hasErrors>

<g:uploadForm id="uploadForm" name="uploadForm" action="save" enctype="multipart/form-data"
	onsubmit="return sfuSubmitForm(this);" method="post">Choose file: <sfu:fileUploadControl />
    <!-- input type="file" name="sfuFile" / -->
<br />
Progress bar: <sfu:fileUploadProgressBar /> <br />
<br />
<input type="submit" value="save"></g:uploadForm>
</body>
</html>