This is a Grails GWT Test Application. 
It uses version 0.5 of the Grails GWT plugin.

To run:

- You need a recent eclipse. STS 2.3.x (or newer) is recommented
  as it has reasonable Groovy and Grails support. Download from
  http://www.springsource.com/products/sts
  
- You need a separate GWT installed (tested with GWT 2.0.3). Download from
  http://code.google.com/webtoolkit/

- The Home directory of your GWT installation should be known to STS:
  Preferences -> Java -> Build Path Variables -> Add 'GWT_HOME'
  Project (GrailsGwtDto) -> Run as ... -> Grails -> Environment -> Add 'GWT_HOME'

- You must compile the GWT client stuff to JavaScript with
  'grails compile-gwt-modules'

- Afterwards, you can run Grails (run-app) from within STS.

- Don't forget to enable JavaScript in your browser ;o)

Main resources
==============

http://www.lazywithclass.com/code/4/gwt-on-grails
Startpage: http://localhost:8088/GrailsGwtDto/bookStore
UiBinder page: http://localhost:8088/GrailsGwtDto/bookStore/uiBinderHelloWorld

http://code.google.com/p/derjanandhisblog/wiki/GWTGrailsTutorial
Startpage: http://localhost:8088/GrailsGwtDto/

http://www.grails.org/plugin/gwt

TreeView:
http://www.grails.org/RichUI+Plugin#TreeView
http://cheztog.blogspot.com/2008/06/yui-treeview-grails.html
Startpage: http://localhost:8088/GrailsGwtDto/fileTree/view

Upload:
http://www.grails.org/plugin/super-file-upload
Startpage: http://localhost:8088/GrailsGwtDto/upload/save

Traditional Grails Scafolded Views:
http://localhost:8088/GrailsGwtDto/author/list
http://localhost:8088/GrailsGwtDto/book/list

Additional resources (untested)
===============================

Command pattern in Grails GWT
-----------------------------
http://www.cacoethes.co.uk/blog/groovyandgrails/the-command-pattern-with-the-grails-gwt-plugin

GWT module concept
------------------
http://developerlife.com/tutorials/?p=124


 