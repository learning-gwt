package com.netzgeneration.grails.util

import javax.servlet.http.HttpServletRequest;

class WebUtil {
	
	private WebUtil() {}
	
	static String requestToString(HttpServletRequest request) {
		final StringBuilder result = new StringBuilder()
		result << 'Request:\n'
		result << ' content length = '
		result << request.contentLength
		result << '\n content type = '
		result << request.contentType
		result << '\n query string = '
		result << request.queryString
		result << '\n request URL = '
		result << request.requestURL
		result << '\nattributes:\n'
		for (an in request.attributeNames) {
			result << an
			result << ' -> '
			result << request.getAttribute(an)
			result << '\n'
		}
		result << "\nparameters:\n"
		for (pn in request.parameterNames) {
			result << pn
			result << ' -> ['
			for (pv in request.getParameter(pn)) {
				result << pv
				result << ', '
			}
			result << ']\n'
		}
		result << "\nattributes:\n"
		for (hn in request.headerNames) {
			result << hn
			result << ' -> ['
			for (hv in request.getHeaders(hn)) {
				result << hv
				result << ', '
			}
			result << ']\n'
		}
		result.toString()
	}

}
