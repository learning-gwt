package com.lazywithclass.client;


import java.util.Date;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.lazywithclass.client.rpc.SimpleRpcService;
import com.lazywithclass.client.rpc.SimpleRpcServiceAsync;
import com.lazywithclass.client.rpc.TransferObject;

public class GrailsGwtDto implements EntryPoint {

	public void onModuleLoad() {
    	Button button = new Button("eh, click me");
    	button.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				SimpleRpcServiceAsync myService = (SimpleRpcServiceAsync) GWT.create(SimpleRpcService.class);
				ServiceDefTarget endpoint = (ServiceDefTarget) myService;
				String moduleRelativeURL = GWT.getModuleBaseURL() + "rpc"; 
				endpoint.setServiceEntryPoint(moduleRelativeURL);
				
				TransferObject to = new TransferObject();
				to.setBar(1);
				to.setBaz(new Date());
				to.setFoo("huah!");
				
				myService.callme(to, new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						Window.alert("failure");
					}

					public void onSuccess(String result) {
						Window.alert("success");
						RootPanel.get().add(new HTML("<p>" + result + "</p>"));
					}
				});
			}
		});
    	
    	RootPanel.get().add(button);
    }
}