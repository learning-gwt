package org.grails.gwttutorial.client;

import java.util.Iterator;
import java.util.Set;

import org.grails.gwttutorial.client.rpc.BookRpcService;
import org.grails.gwttutorial.client.rpc.BookRpcServiceAsync;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Application implements EntryPoint {
	
	private BookRpcServiceAsync bookService;

	private Tree tree;

	public void onModuleLoad() {

		tree = new Tree();

		bookService = (BookRpcServiceAsync) GWT.create(BookRpcService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) bookService;
		String moduleRelativeUrl = GWT.getModuleBaseURL() + "rpc";
		endpoint.setServiceEntryPoint(moduleRelativeUrl);

		bookService.getAuthor(1, new AsyncCallback<String>() {

			public void onFailure(Throwable arg0) {
				Window.alert("failure");
			}

			public void onSuccess(String author) {
				JSONValue value = JSONParser.parse(author);
				tree.removeItems();
				tree.setVisible(true);
				TreeItem item = tree.addItem("Response");
				addChildren(item, value);
			}

		});

		RootPanel panel = RootPanel.get("bookStoreContainer");
		if (panel != null) {
			panel.add(tree);
		}
		// Added for UiBinderHelloWorld
		panel = RootPanel.get("uiBinderHelloWorld");
		if (panel != null) {
			UiBinderHelloWorld helloWorld = new UiBinderHelloWorld();
			Document.get().getBody().appendChild(helloWorld.getElement());
			// panel.add(helloWorld);
			helloWorld.setName("World");
		}
	}

	private void addChildren(TreeItem treeItem, JSONValue jsonValue) {
		JSONArray jsonArray;
		JSONObject jsonObject;
		JSONString jsonString;

		if ((jsonArray = jsonValue.isArray()) != null) {
			for (int i = 0; i < jsonArray.size(); ++i) {
				TreeItem child = treeItem.addItem(getChildText("["
						+ Integer.toString(i) + "]"));
				addChildren(child, jsonArray.get(i));
			}
		} else if ((jsonObject = jsonValue.isObject()) != null) {
			Set<String> keys = jsonObject.keySet();
			for (Iterator<String> iter = keys.iterator(); iter.hasNext();) {
				String key = iter.next();
				TreeItem child = treeItem.addItem(getChildText(key));
				addChildren(child, jsonObject.get(key));
			}
		} else if ((jsonString = jsonValue.isString()) != null) {
			treeItem.addItem(jsonString.stringValue());
		} else {
			treeItem.addItem(getChildText(jsonValue.toString()));
		}
	}

	private String getChildText(String text) {
		return "<span style='white-space:normal'>" + text + "</span>";
	}
	
}
