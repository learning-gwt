package org.grails.gwttutorial.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.UIObject;

/**
 * @see {@link http://code.google.com/webtoolkit/doc/latest/DevGuideUiBinder.html}
 */
public class UiBinderHelloWorld extends UIObject { // Could extend Widget instead

	interface MyUiBinder extends UiBinder<DivElement, UiBinderHelloWorld> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	@UiField
	SpanElement uiBinderHelloWorld;

	public UiBinderHelloWorld() {
		// createAndBindUi initializes this.nameSpan
		setElement(uiBinder.createAndBindUi(this));
	}

	public void setName(String name) {
		uiBinderHelloWorld.setInnerText(name);
	}
	
}
