package com.lazywithclass.client.rpc;

import com.google.gwt.user.client.rpc.RemoteService;

public interface SimpleRpcService extends RemoteService {
    String callme(TransferObject to);
}