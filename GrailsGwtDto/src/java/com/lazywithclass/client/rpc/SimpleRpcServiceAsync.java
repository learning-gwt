package com.lazywithclass.client.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SimpleRpcServiceAsync {
    void callme(TransferObject to, AsyncCallback<String> callback);
}