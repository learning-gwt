package com.lazywithclass.client.rpc;

import java.io.Serializable;
import java.util.Date;

public class TransferObject implements Serializable {

	private static final long serialVersionUID = 5568580340719801498L;
	private String foo;
	private Integer bar;
	private Date baz;
	
	public String getFoo() {
		return foo;
	}
	public void setFoo(String foo) {
		this.foo = foo;
	}
	public Integer getBar() {
		return bar;
	}
	public void setBar(Integer bar) {
		this.bar = bar;
	}
	public Date getBaz() {
		return baz;
	}
	public void setBaz(Date baz) {
		this.baz = baz;
	}
}