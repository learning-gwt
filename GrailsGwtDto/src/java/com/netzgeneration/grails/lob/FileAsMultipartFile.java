package com.netzgeneration.grails.lob;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public class FileAsMultipartFile implements MultipartFile {
	
	private final File wrapped;
	
	private final String contentType;
	
	public FileAsMultipartFile(File wrapped, String contentType) {
		if (wrapped == null) throw new NullPointerException();
		this.wrapped = wrapped;
		this.contentType = contentType;
	}

	@Override
	public byte[] getBytes() throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return new BufferedInputStream(new FileInputStream(wrapped));
	}

	@Override
	public String getName() {
		return wrapped.getName();
	}

	@Override
	public String getOriginalFilename() {
		return wrapped.getName();
	}

	@Override
	public long getSize() {
		return wrapped.length();
	}

	@Override
	public boolean isEmpty() {
		return getSize() == 0L;
	}

	@Override
	public void transferTo(File file) throws IOException, IllegalStateException {
		wrapped.renameTo(file);
	}
	
	@Override
	public String toString() {
		return "FileAsMultipart@" + wrapped;
	}

}
