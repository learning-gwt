package org.grails.gwttutorial.client.rpc;

import com.google.gwt.user.client.rpc.RemoteService;

public interface BookRpcService extends RemoteService {
	
	String getAuthor(int id);

}
