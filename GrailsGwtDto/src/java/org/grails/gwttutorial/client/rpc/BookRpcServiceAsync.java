package org.grails.gwttutorial.client.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BookRpcServiceAsync {

	void getAuthor(int id, AsyncCallback<String> callback);

}
